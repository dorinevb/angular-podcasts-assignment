export interface Podcast {
  title: string;
  author: string;
  image: string;
}