import { Component, Input } from '@angular/core';
import { Podcast } from 'src/app/models/podcast.model';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts.component.html',
  styleUrls: ['./podcasts.component.scss']
})
export class PodcastsComponent {

  @Input()
  podcasts: Podcast[] = []

  constructor() { }
}
