import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PodcastsPage } from './pages/podcasts/podcasts.page';
import { PodcastGenresPage } from './pages/podcast-genres/podcast-genres.page';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/podcasts"
  },
  {
    path: "podcasts",
    component: PodcastsPage
  },
  {
    path: "podcast-genres",
    component: PodcastGenresPage
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
