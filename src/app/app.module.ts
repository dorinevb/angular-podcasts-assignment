import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PodcastsComponent } from './components/podcasts/podcasts.component';
import { PodcastsPage } from './pages/podcasts/podcasts.page';
import { PodcastGenresPage } from './pages/podcast-genres/podcast-genres.page';

@NgModule({
  declarations: [
    AppComponent,
    PodcastsComponent,
    PodcastsPage,
    PodcastGenresPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
