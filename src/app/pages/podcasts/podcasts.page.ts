import { Component, OnInit } from '@angular/core';
import { Podcast } from '../../models/podcast.model';


@Component({
  selector: 'app-podcasts-page',
  templateUrl: './podcasts.page.html',
  styleUrls: ['./podcasts.page.scss']
})
export class PodcastsPage implements OnInit {

  public podcasts: Podcast[] = [
    /* Image by Vespertilio, on 99desings.no */
    {
      title: "Unconventional Wisdom",
      author: "unknown",
      image: "https://images-workbench.99static.com/C3Rp221EyITwO91_Sm9W5xxEGBw=/99designs-contests-attachments/99/99684/attachment_99684469"
    },
    /* Image by KaWolfram, on 99desings.no */
    {
      title: "Couch Chronicles",
      author: "also unknown",
      image: "https://images-workbench.99static.com/lhxCuRqIhry0OPjL58e55UEhVXs=/99designs-contests-attachments/88/88067/attachment_88067437"
    },
    /* Image by RockNDraw, on 99desings.no */
    {
      title: "Are we bored yet?",
      author: "I don't know",
      image: "https://images-workbench.99static.com/O5464cN2jbIOmJmF3TzulaAsr-M=/99designs-contests-attachments/98/98890/attachment_98890907"
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
